from urllib.request import urlopen
from html.parser import HTMLParser
from subprocess import run, PIPE, DEVNULL, TimeoutExpired
from string import ascii_uppercase
from tempfile import NamedTemporaryFile
from string import Formatter
import sys, os, glob, time, re, json, io
import readline

os.chdir(os.path.realpath(os.path.dirname(__file__))) # Set root dir to script's  location

platforms = [
    {
        'platform':   'codeforces',
        'id':         '{contest}{problem}',
        'id_parser': {
            'contest': '[0-9]+',
            'problem': '[A-Z]',
        },
        'url': 'http://codeforces.com/contest/{contest}/problem/{problem}',
        'downloader': lambda problem: codeforces_downloader(problem),
    },
    {
        'platform': 'codejam',
        'id':       '{year}{round}{problem}',
        'source':   '{path}/solution.{extension}',
        'id_parser': {
            'year': '[0-9]+',
            'round': 'QR|RA|RB|RC|R2|R3|WF',
            'problem': '[1-9]',
        },
    },
    {
        'platform':  'uva',
        'id':        '{group}{number}',
        'statement': '{path}/{id}.html',
        'id_parser': {
            'group':  '[0-9]+(?=\\d)',
            'number': '[0-9][0-9]',
        },
        'url': 'http://uva.onlinejudge.org/external/{group}/{id}.pdf',
    },
    {
        'platform':  'icpc_live',
        'id':        '{group}{number}',
        'statement': '{path}/{id}.html',
        'id_parser': {
            'group':  '[0-9]+(?=\\d)',
            'number': '[0-9][0-9]',
        },
        'url': 'https://icpcarchive.ecs.baylor.edu/external/{group}/{id}.pdf',
    },
]

langs = [
    {
        'lang':      'pypy3',
        'extension': 'py',
        'compiler':  'python3 -m py_compile {source} && rm -r {path}/__pycache__',
        'executer':  'pypy3 {source}',
        'template':  'templates/template.{extension}',
    },
    {
        'lang':      'python3',
        'extension': 'py',
        'compiler':  'python3 -m py_compile {source} && rm -r {path}/__pycache__',
        'executer':  'python3 {source}',
        'template':  'templates/template.{extension}',
    },
    {
        'lang':      'c++11',
        'extension': 'cpp',
        'compiler':  'g++ -std=c++11 -o {tmp_program} {source}',
        'executer':   '{tmp_program}',
        'template':  'templates/template.{extension}',
    },
    {
        'lang':      'c++5',
        'extension': 'cpp',
        'compiler':  'g++ -o {tmp_program} {source}',
        'executer':  '{tmp_program}',
        'template':  'templates/template.{extension}',
    },
    {
        'lang':      'kotlin',
        'extension': 'kt',
        'compiler':  'kotlinc {source} -include-runtime -d {tmp_program}.jar',
        'executer':  'java -jar {tmp_program}.jar',
        'template':  'templates/template.{extension}',
    },
]


def os_exec(cmd, verbose=True, max_output='500M', timeout=None):
    '''
    Execute a shell command.
    if verbose: print to stdout (immediately after each flush)
    Output may have a max_output safety size
    Execution may have a timeout
    return stdout string, stderr string and elapsed time float
    '''
    with NamedTemporaryFile(suffix='.out') as tmp:
        tmp_out = tmp.name
    try:
        cmd = f'{{ {cmd}  || echo Non zero exit code $? >&2; }}'
        if max_output!=None:
            cmd = f'{cmd} | stdbuf -o0 head -c {max_output}'
        cmd = f'{cmd} | tee {tmp_out}'
        start = time.time()
        end = None
        stderr = stdout = ''
        try:
            p = run(cmd, shell=True, check=True,
                stderr=PIPE,
                stdout=None if verbose else DEVNULL,
                timeout=timeout
            )
            end = time.time()
            stderr = p.stderr.decode().strip()
        except KeyboardInterrupt as e:
            stderr = '\nStopped. KeyboardInterrupt'
        except TimeoutExpired as e:
            stderr = f'Stopped. Reached timeout of {timeout}s'
            end = start + timeout + 1e-6
        except Exception as e:
            stderr = str(e)
        end = end or time.time()
        stdout = run(['cat', tmp_out], stdout=PIPE).stdout.decode()
    finally:
        if os.path.exists(tmp_out):
            run(['rm', tmp_out])
    return stdout, stderr, end-start


def xdg_open(filename):
    run(['touch', filename])
    return run(['xdg-open', filename], stdout=DEVNULL, stderr=DEVNULL)

def read_file(fname):
    with open(fname) as f: 
        content = f.read()
    return content

def rlinput(prompt, prefill='', check=None, msg=None):
    if check==None:
        check = lambda s: True
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    first = True
    while 1:
        try: s = input(prompt)
        finally: readline.set_startup_hook()
        if check(s):
            break
        if msg!=None:
            print(msg)
    return s

def my_order(text):
    atoi = lambda x: int(x) if x.isdigit() else x
    return [atoi(x) for x in re.split(r'(\d+)', text)]

def bool_input(prompt, prefill='', default=None):
    def parse(s):
        if s.lower().startswith('y'): return True
        if s.lower().startswith('n'): return False
        if not s: return default
        return None
    ans = rlinput(prompt, prefill=prefill,
        check=lambda s: parse(s)!=None,
        msg='Please type yes or no'
    )
    return parse(ans)


class Problem:
    platform_defaults = {
        'path':       '{platform}/{id}',
        'source':     '{path}/{id}.{extension}',
        'sample_in':  '{path}/sample{num}.in',
        'sample_out': '{path}/sample{num}.out',
        'statement':  '{path}/statement.html',
    }

    platforms = platforms
    langs = langs
    dplatforms = {x['platform']:x for x in platforms}
    dlangs = {x['lang']:x for x in langs}

    def __init__(self, platform, lang, problem_id, time_limit=None, max_output='500M'):
        attrs = {
            **Problem.platform_defaults,
            **Problem.parse_problem_id(platform, problem_id),
            **Problem.dplatforms.get(platform),
            **Problem.dlangs.get(lang),
        }
        nostr = {k:v for k,v in attrs.items() if not isinstance(v, str)}
        attrs = {k:v for k,v in attrs.items() if isinstance(v, str)}

        # Toposort string dependencies
        topo = []
        R = {k:[] for k in attrs}
        depdeg = {k:0 for k in attrs}
        for k in attrs:
            for d in attrs:
                if '{'+d+'}' in attrs[k]:
                    depdeg[k]+=1
                    R[d].append(k)
        z = [k for k in attrs if depdeg[k]==0]
        while z:
            u = z.pop()
            topo.append(u)
            for v in R.pop(u):
                depdeg[v] -= 1
                if depdeg[v]==0:
                    z.append(v)
        circular = [k for k in attrs if k not in topo]
        assert not circular, f'Circular references: {circular}\n{attrs}'

        # Replace dependencies and save to self
        norep = ['num', 'tmp_program'] # future replacement, dont replace now
        for k in norep:
            attrs[k] = '{'+k+'}'
        for k in topo:
            attrs[k] = attrs[k].format_map(attrs)
        for k in norep:
            del attrs[k]

        for key,value in attrs.items():
            setattr(self, key, value)
        for key,value in nostr.items():
            setattr(self, key, value)

        self.time_limit = time_limit
        self.max_output = max_output

    def update(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)

    @staticmethod
    def parse_problem_id(platform, problem_id):
        platform = Problem.dplatforms.get(platform)
        if platform==None: return None
        regex = platform['id']
        replace = {k: f'(?P<{k}>{v})' for k, v in platform['id_parser'].items()}
        try: regex = regex.format(**replace)
        except KeyError: return None
        #print(regex)
        regex = re.compile(regex).fullmatch(problem_id)
        if regex==None: return None
        return regex.groupdict()

    def format(self, fmt_str, **kwargs):
        return fmt_str.format(**vars(self), **kwargs)

    def __repr__(self):
        return self.format('{platform} {id} ({lang})')

    def execute(self, sample_in, tmp_program=None, verbose=True):
        assert tmp_program or not self.compiler, 'You must compile first'
        cmd_exec = self.format(self.executer, tmp_program=tmp_program)
        cmd = f'{cmd_exec} < {sample_in}'
        stdout, stderr, exec_time = os_exec(
            cmd,
            verbose=verbose,
            timeout=self.time_limit,
            max_output=self.max_output
        )
        if verbose and stderr:
            print(stderr)
        sample_out = sample_in[:-3]+'.out'
        if self.time_limit!=None and exec_time >= self.time_limit:
            veredict = 'TL'
        elif stderr:
            veredict = 'RE'
        elif not os.path.exists(sample_out):
            veredict = '¿?'
        elif stdout==read_file(sample_out):
            veredict = 'AC'
        else:
            veredict = 'WA'
        return veredict, exec_time

    def run(self, inputs, summary=False):
        with NamedTemporaryFile() as program:
            tmp_program = program.name
        try:
            cmdc = self.format(self.compiler, tmp_program=tmp_program)
            try:
                MyProcess(cmdc, shell=True, check=True)
            except:
                print(f'\nCompilation failed ({self.lang}).\n   Command: {cmdc}')
                return
            print(f'Compilation ok ({self.lang}): {cmdc}')

            cmdx = self.format(self.executer, tmp_program=tmp_program)
            print(f'Execution line ({self.lang}): {cmdx}')
            if summary:
                print()
                print('Execution summary:')
                print('    Judgements:')
            else:
                print('-'*20)

            total_time = 0
            for sample_in in inputs:
                if not summary:
                    print(f'Input: {sample_in}')
                veredict, exec_time = self.execute(
                    sample_in,
                    tmp_program=tmp_program,
                    verbose=not summary,
                )
                total_time += exec_time
                if not summary:
                    print(f'Judgement: {veredict} ({exec_time:.2f} s)')
                    print('-'*20)
                else:
                    print(f'       {veredict} ({exec_time:.2f} s) for {sample_in}')
            if not inputs:
                print('No inputs found')
            if summary:
                print(f'    Total time:  {total_time:.2f} s')
        finally:
            if os.path.exists(tmp_program):
                run(['rm', tmp_program])
        return


class InteractiveMenu(Problem):
    cache_file = '.amigo_cp.json'

    def __init__(self):
        try: loaded = json.loads(read_file(self.cache_file))
        except: loaded = {}
        
        default = dict(platform='codeforces', lang='pypy3', problem_id='1342A')
        init = self.init_ask(**{k: loaded.get(k, v) for k,v in default.items()})
        
        super().__init__(**init)
        
        with open(self.cache_file, 'w') as f:
            f.write(json.dumps(init, indent=4))

        if not os.path.exists(self.path):
            run(['mkdir', '-p', self.path])
        if not os.path.exists(self.source):
            self.create_source()
        if not os.path.exists(self.statement):
            self.download()
        else:
            if bool_input('Would you like to open the statement [y/n]? '):
                xdg_open(self.statement)
        return

    def init_ask(self, platform, lang, problem_id):
        self.cls()
        print('Welcome to amigo_cp! Your competitive programming friend...\n')
        pmsg = 'Supported platforms: '+' '.join(Problem.dplatforms)
        lmsg = 'Supported languages: '+' '.join(Problem.dlangs)
        print(pmsg+'\n'+lmsg+'\n')
        lang = rlinput('    Language: ', lang or '',
            check=Problem.dlangs.__contains__,
            msg=lmsg
        )
        platform = rlinput('    Platform: ', platform or '',
            check=Problem.dplatforms.__contains__,
            msg=pmsg
        )
        p = Problem.dplatforms[platform]
        xmsg = f'\nInvalid format.\nExpected for {platform}: '+str(p['id'])
        xmsg += '\n'+'\n'.join(f'    {k}: {v}' for k,v in p['id_parser'].items())+'\n'
        problem_id = rlinput('    Problem id: ', problem_id or '',
            check=lambda s: Problem.parse_problem_id(platform, s),
            msg=xmsg
        )
        return dict(platform=platform, lang=lang, problem_id=problem_id)

    def cls(self):
        os.system('cls' if os.name=='nt' else 'clear')

    def print_menu(self, extended=False):
        print(self.format('\nProblem {id} ({lang}) {source}'))
        if self.time_limit!=None:
            print(self.format('Custom time limit: {time_limit}'))
        print()
        print('    [#]  Run test case number #. Use 0 to run all, s for summary.')
        print('    [l]  List existing test cases, and check locally for new ones')
        print('    [m]  More...')
        print('    [q]  Quit')
        if extended:
            print('    [c]  Change      change problem / language')
            print('    [d]  Download    download statement and test cases. Overwrites')
            print('    [e]  Edit:       open source code')
            print('    [f]  Folder:     open problem folder')
            print('    [i#] Input:      create/open input file number #. No overwrite.')
            print('    [o#] Output:     create/open output file number #. No overwrite.')
            print('    [s]  Summary:    run all testcases hiding output.')
            print('    [st] Statement:  open statement.')
            print('    [t#] Time limit: set the time limit to # seconds. 0 for none.')
            print('    [enter] Clear')
        return

    def main(self):
        x = '' ; prefill=''
        self.cls()
        self.print_menu()
        inputs, empty = self.ls_inputs()
        extended = False
        while 1:
            if x=='#':
                print('Please enter a number, not the symbol #')
            elif x=='':
                self.cls()
                self.print_menu()
            elif x=='0':
                self.cls()
                self.run(inputs, summary=False)
            elif x=='s':
                self.cls()
                self.run(inputs, summary=True)
            elif x=='st':
                if os.path.exists(self.statement):
                    xdg_open(self.statement)
                else:
                    print('No statement yet. Use the download tool first [d]')
                    prefill = 'd'
            elif x.isdigit():
                i = int(x)-1
                if i<len(inputs):
                    self.cls()
                    self.run([inputs[i]], summary=False)
                else:
                    print('Invalid test case number')
            elif x=='l':
                self.cls()
                self.print_menu()
                inputs, empty = self.ls_inputs()
                for i,f in enumerate(inputs):
                    print(f'    [{1+i}]  {f}')
                for f in empty:
                    print(f'    [-] {f} (ignored, empty)')
            elif x=='m':
                self.cls()
                self.print_menu(extended=True)
            elif x=='c':
                self.__init__()
                self.cls()
                self.print_menu()
                inputs, empty = self.ls_inputs()
            elif x=='d':
                self.download()
            elif x=='e':
                print(f'Opening {self.source}...')
                xdg_open(self.source)
            elif x=='f':
                print(f'Opening {self.path}...')
                run(['mkdir', '-p', self.path])
                xdg_open(self.path)
            elif x.startswith('i') or x.startswith('o'):
                user = x[1:].strip()
                num = -1
                try: num = int(user) ; assert num>0
                except: print('Invalid problem value. Enter a positive integer')
                if num>0:
                    self.cls()
                    self.print_menu()
                    if x.startswith('i'):
                        fname = self.format(self.sample_in, num=num)
                    else:
                        fname = self.format(self.sample_out, num=num)
                    print(f'Opening {fname}...')
                    xdg_open(fname)
            elif x.startswith('t'):
                user = x[1:].strip()
                t = -1
                try: t = float(user) ; assert t >=0
                except: print('Invalid time value. Enter a positive float')
                if t>=0:
                    self.time_limit = t or None
                    self.cls()
                    self.print_menu()
            elif x=='q':
                return
            else:
                print('Unknown option:', x)
            x = rlinput('\nChoose [#, l, m, ...]: ', prefill=prefill).lower().strip()
            prefill = ''

    def ls_inputs(self):
        inputs = glob.glob(self.format('{path}/*.in'))
        empty = [f for f in inputs if not read_file(f).strip()]
        inputs = [f for f in inputs if f not in empty]
        inputs = sorted(inputs, key=lambda f: ('sample' not in f, my_order(f)))
        return inputs, empty

    def create_source(self):
        print(f'Creating {self.source} from template:')
        if os.path.exists(self.source):
            print('    Skipping. File already exists.')
        elif os.path.exists(self.template):
            print(f'    Copying {self.template}')
            run(['cp', self.template, self.source], check=True)
        else:
            print(f'    Setting empty file: template not found {self.template}')
            run(['touch', self.source], check=True)
        return

    def download(self):
        # Get url
        if hasattr(self, 'url'):
            url = self.url
        else:
            prefill = ''
            if os.path.exists(self.statement):
                prefill = read_file(self.statement)
                prefill = prefill[prefill.find('url='):]
                prefill = prefill[4:prefill.find('"')]
            url = rlinput(
                '    Type the problem url (html or pdf) or q to cancel: ',
                prefill=prefill,
                check=lambda s: s.lower()=='q' or (s.startswith('http') and ' ' not in s.strip()), 
                msg='URL can not contain spaces and must start with http or https',
            ).strip()
            if url.lower()=='q':
                return

        # Save light statement file
        print('\nCreating statement quick access...')
        print(f'    Source:      {url}')
        print(f'    Destination: {self.statement}')
        stmt = '<html><head><meta http-equiv="refresh" content="0; url={url}"/></head></html>'
        with open(self.statement, 'w') as f:
            f.write(stmt.format(url=url))

        # Save samples
        if hasattr(self, 'downloader'):
            print(f'Dowloading sample io...')
            print(f'    Source:      {url}')
            print(f'    Destination: {self.sample_in}')
            print(f'                 {self.sample_out}')
            self.downloader(self)
        else:
            self.cls()
            print('URL:', url)
            print(f'\n    No download script found for {self.platform}')
            print(f'    I will help you to copy/paste the test cases manually.\n')

            if bool_input(f'    Open statement [y/n]? '):
                xdg_open(self.statement)
            for num in range(1, 100):
                if not bool_input(f'    Open sample in/out {num} [y/n]? '):
                    break
                xdg_open(self.sample_out.format(num=num))
                xdg_open(self.sample_in.format(num=num))
        return


def codeforces_downloader(problem):
    num = CodeforcesProblemParser().download_sample_io(
        url=problem.url,
        sample_in=problem.sample_in,
        sample_out=problem.sample_out,
    )
    print(f'    Sucess:     {num} samples downloaded')
    return


# https://github.com/johnathan79717/codeforces-parser/blob/master/parse.py
class CodeforcesProblemParser(HTMLParser):

    def download_sample_io(self, url, sample_in, sample_out):
        self.sample_in = sample_in
        self.sample_out = sample_out
        self.num_tests = 0
        self.testcase = None
        self.start_copy = False
        html = urlopen(url).read()
        self.feed(html.decode('utf-8'))
        # .encode('utf-8') Should fix special chars problems?
        return self.num_tests

    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            if attrs == [('class', 'input')]:
                self.num_tests += 1
                fname = self.sample_in.format(num=self.num_tests)
                self.testcase = open(fname, 'wb')
            elif attrs == [('class', 'output')]:
                fname = self.sample_out.format(num=self.num_tests)
                self.testcase = open(fname, 'wb')
        elif tag == 'pre':
            if self.testcase != None:
                self.start_copy = True

    def handle_endtag(self, tag):
        if tag == 'br':
            if self.start_copy:
                self.testcase.write('\n'.encode('utf-8'))
                self.end_line = True
        if tag == 'pre':
            if self.start_copy:
                if not self.end_line:
                    self.testcase.write('\n'.encode('utf-8'))
                self.testcase.close()
                self.testcase = None
                self.start_copy = False

    def handle_entityref(self, name):
        if self.start_copy:
            self.testcase.write(self.unescape(('&%s;' % name)).encode('utf-8'))

    def handle_data(self, data):
        if self.start_copy:
            self.testcase.write(data.strip('\n').encode('utf-8'))
            self.end_line = False


InteractiveMenu().main()
