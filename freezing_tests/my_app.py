from flexx import flx

class Example(flx.Widget):
    def init(self):
        flx.Button(text="Hi there")

if __name__=='__main__':
    app = flx.App(Example)
    app.launch("firefox-app")
    flx.run()