from urllib.request import urlopen
from html.parser import HTMLParser
import asyncio, threading

platforms = [
    {
        'platform':   'codeforces',
        'id':         '{contest}{problem}',
        'id_parser': {
            'contest': '[0-9]+',
            'problem': '[A-Z]',
        },
        'url': 'http://codeforces.com/contest/{contest}/problem/{problem}',
    },
    {
        'platform': 'codejam',
        'id':       '{year}{round}{problem}',
        'source':   '{path}/solution.{extension}',
        'id_parser': {
            'year': '[0-9]+',
            'round': 'QR|RA|RB|RC|R2|R3|WF',
            'problem': '[1-9]',
        },
    },
    {
        'platform':  'uva',
        'id':        '{group}{number}',
        'statement': '{path}/{id}.html',
        'id_parser': {
            'group':  '[0-9]+(?=\\d)',
            'number': '[0-9][0-9]',
        },
        'url': 'http://uva.onlinejudge.org/external/{group}/{id}.pdf',
    },
    {
        'platform':  'icpc_live',
        'id':        '{group}{number}',
        'statement': '{path}/{id}.html',
        'id_parser': {
            'group':  '[0-9]+(?=\\d)',
            'number': '[0-9][0-9]',
        },
        'url': 'https://icpcarchive.ecs.baylor.edu/external/{group}/{id}.pdf',
    },
]

languages = [
    {
        'language':  'pypy3',
        'extension': 'py',
        'compiler':  'python3 -m py_compile {source} && rm -r {path}/__pycache__',
        'executer':  'pypy3 {source}',
        'template':  'templates/template.{extension}',
    },
    {
        'language':  'python3',
        'extension': 'py',
        'compiler':  'python3 -m py_compile {source} && rm -r {path}/__pycache__',
        'executer':  'python3 {source}',
        'template':  'templates/template.{extension}',
    },
    {
        'language':  'c++11',
        'extension': 'cpp',
        'compiler':  'g++ -std=c++11 -o {tmp_program} {source}',
        'executer':   '{tmp_program}',
        'template':  'templates/template.{extension}',
    },
    {
        'language':  'c++5',
        'extension': 'cpp',
        'compiler':  'g++ -o {tmp_program} {source}',
        'executer':  '{tmp_program}',
        'template':  'templates/template.{extension}',
    },
    {
        'language':  'kotlin',
        'extension': 'kt',
        'compiler':  'kotlinc {source} -include-runtime -d {tmp_program}.jar',
        'executer':  'java -jar {tmp_program}.jar',
        'template':  'templates/template.{extension}',
    },
] 

async def async_urlopen_read(url):
    sync = asyncio.Event()
    aux = [None]
    def thread():
        aux[0] = urlopen(url).read()
        sync.set()
        return
    threading.Thread(target=thread).start()
    await sync.wait()
    return aux[0]


async def codeforces_downloader(problem):
    sample_in = problem.sample.replace('{in_or_out}', 'in')
    sample_out = problem.sample.replace('{in_or_out}', 'out')
    num = await CodeforcesProblemParser().download_sample_io(
        url=problem.url,
        sample_in=sample_in,
        sample_out=sample_out,
    )
    print(f'    Sucess:     {num} samples downloaded')
    return


# https://github.com/johnathan79717/codeforces-parser/blob/master/parse.py
class CodeforcesProblemParser(HTMLParser):

    async def download_sample_io(self, url, sample_in, sample_out):
        self.sample_in = sample_in
        self.sample_out = sample_out
        self.num_tests = 0
        self.testcase = None
        self.start_copy = False
        html = await async_urlopen_read(url)
        self.feed(html.decode('utf-8'))
        # .encode('utf-8') Should fix special chars problems?
        return self.num_tests

    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            if attrs == [('class', 'input')]:
                self.num_tests += 1
                fname = self.sample_in.format(num=self.num_tests)
                self.testcase = open(fname, 'wb')
            elif attrs == [('class', 'output')]:
                fname = self.sample_out.format(num=self.num_tests)
                self.testcase = open(fname, 'wb')
        elif tag == 'pre':
            if self.testcase != None:
                self.start_copy = True

    def handle_endtag(self, tag):
        if tag == 'br':
            if self.start_copy:
                self.testcase.write('\n'.encode('utf-8'))
                self.end_line = True
        if tag == 'pre':
            if self.start_copy:
                if not self.end_line:
                    self.testcase.write('\n'.encode('utf-8'))
                self.testcase.close()
                self.testcase = None
                self.start_copy = False

    def handle_entityref(self, name):
        if self.start_copy:
            self.testcase.write(self.unescape(('&%s;' % name)).encode('utf-8'))

    def handle_data(self, data):
        if self.start_copy:
            self.testcase.write(data.strip('\n').encode('utf-8'))
            self.end_line = False
